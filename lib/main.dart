import 'package:flutter/material.dart';
import 'package:laundrybee/pages/bantuan.dart';
import 'package:laundrybee/pages/home.dart';
import 'package:laundrybee/pages/laporan.dart';
import 'package:laundrybee/pages/setting.dart';
import 'package:laundrybee/pages/toko.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedIndex = 0;

  final _layoutPage = [Home(), Toko(), Bantuan(), Laporan(), Setting()];

  void _onTabItem(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _layoutPage.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(Icons.home), title: Text('Home')),
          BottomNavigationBarItem(icon: Icon(Icons.save), title: Text('Toko')),
          BottomNavigationBarItem(
              icon: Icon(Icons.view_agenda), title: Text('Bantuan')),
          BottomNavigationBarItem(
              icon: Icon(Icons.inbox), title: Text('Laporan')),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_circle), title: Text('Setting')),
        ],
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedIndex,
        onTap: _onTabItem,
      ),
    );
  }
}
